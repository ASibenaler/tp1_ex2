#!/usr/bin/env python
# coding : utf-8

class SimpleComplexCalculator:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def fsum(self):
        return [self.a[0] + self.b[0], self.a[1] + self.b[1]]

    def substract(self):
        return [self.a[0] - self.b[0], self.a[1] - self.b[1]]

    def multiply(self):
        return [
            self.a[0] * self.b[0] - self.a[1] * self.b[1],
            self.a[0] * self.b[1] + self.a[1] * self.b[0],
        ]

    def divide(self):
        if (self.b[0] ** 2 + self.b[1] ** 2) != 0:
            return [
                (self.a[0] * self.b[0] + self.a[1] * self.b[1])
                / (self.b[0] ** 2 + self.b[1] ** 2),
                (self.a[1] * self.b[0] + self.a[0] * self.b[1])
                / (self.b[0] ** 2 + self.b[1] ** 2),
            ]
        else:
            return "Division par 0 !"


if __name__ == "__main__":
    TEST = SimpleComplexCalculator([1, 2], [3, 4])
    print("Somme : ", TEST.fsum())
    print("Soustraction : ", TEST.substract())
    print("Produit : ", TEST.multiply())
    print("Division : ", TEST.divide())
    
