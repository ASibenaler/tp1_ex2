## Exercice 2

# Objectif
L'objectif de cet exercice est de réaliser une classe SimpleComplexCalculator proposant les 4 méthodes de calcul de complexes.

# Réalisation
On créé une classe SimpleComplexCalculator contenant les 4 méthodes de calcul, ainsi qu'une méthode *__init__* initialisant ses attributs.
Une fonction *main* permet de tester cette classe en l'instanciant et en appelant ses méthodes.
